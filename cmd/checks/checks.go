package checks

func CheckErrors(e error) {
	if e != nil {
		panic(e)
	}
}
