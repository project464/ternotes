package out

import (
	"fmt"
	"os"
	"ternotes/cmd/types"
)

func ShowNotesForTag(nc types.NoteCollection) {
	if len(nc.Notes) == 0 {
		fmt.Println("\tДля вашего тега записи не доступны\n")
		fmt.Println("~~~")
	} else {

		fmt.Println("\n\tДля тега ", nc.Notes[0].Tag, "доступны следующие записи\n")
		for _, n := range nc.Notes {
			fmt.Println(n.ID, ":", n.Command)
			fmt.Println()
			fmt.Println(n.Description)
			fmt.Println("---------------------------")
		}
	}
}

func ShowTags(n []string) {
	fmt.Println("\tДоступные теги:\n")
	for index, value := range n {

		fmt.Print(index+1, ": ")
		fmt.Println(value)
	}
	fmt.Println("\n~~~")
}

func ShowDeletedID(result int64) {
	if result == 0 {
		fmt.Println("Удалено ", result, "ячеек!")
	} else {
		fmt.Println("Удалено ", result, "ячейка!")
	}
}

func PasteCommandToConsole(result string) {
	fmt.Fprintln(os.Stdout, result)
}

func ShowDeletedTag(result int64) {
	fmt.Println("Удалено ", result, "ячеек!")
}

func WrongArguments()   {}
func TooMuchArguments() {}
