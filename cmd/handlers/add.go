package handlers

import (
	"bufio"
	"database/sql"
	"fmt"
	"os"
	"strings"
	"ternotes/cmd/checks"
)

func Add(db *sql.DB) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter tag: ")
	tag, _ := reader.ReadString('\n')
	tag = strings.Replace(tag, "\n", "", -1)
	fmt.Print("Enter command: ")
	command, _ := reader.ReadString('\n')
	command = strings.Replace(command, "\n", "", -1)
	fmt.Print("Enter descirption: ")
	description, _ := reader.ReadString('\n')
	description = strings.Replace(description, "\n", "", -1)

	stmt, err := db.Prepare("INSERT INTO notes (tag, command, description) VALUES(?, ?, ?)")
	checks.CheckErrors(err)
	result, err := stmt.Exec(tag, command, description)
	checks.CheckErrors(err)

	insertedId, err := result.LastInsertId()
	checks.CheckErrors(err)

	fmt.Println("Новая запись c id", insertedId, "добавленна.")

}
