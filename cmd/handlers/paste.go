package handlers

import (
	"database/sql"
	"ternotes/cmd/checks"
	"ternotes/cmd/out"
	"ternotes/cmd/types"
)

func PasteCommand(id int, db *sql.DB) {

	sql := "Select command FROM notes WHERE id = ?"

	rows, err := db.Query(sql, id)
	checks.CheckErrors(err)
	defer rows.Close()

	for rows.Next() {
		note := types.Note{}

		err := rows.Scan(&note.Command)
		checks.CheckErrors(err)

		out.PasteCommandToConsole(note.Command)
	}
}
