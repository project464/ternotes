package handlers

import (
	"database/sql"
	"ternotes/cmd/checks"
	"ternotes/cmd/out"
	"ternotes/cmd/types"
)

func List(tag string, db *sql.DB) {
	switch tag {
	case "":
		listWithNoTag(db)
	default:
		listWithTag(tag, db)
	}

}

func listWithTag(tag string, db *sql.DB) {
	sql := "Select * from notes where tag=?"

	rows, err := db.Query(sql, tag)
	checks.CheckErrors(err)
	defer rows.Close()

	if rows != nil {

		result := types.NoteCollection{}

		for rows.Next() {
			note := types.Note{}

			err := rows.Scan(&note.ID, &note.Tag, &note.Command, &note.Description)
			checks.CheckErrors(err)

			result.Notes = append(result.Notes, types.Note{
				note.ID,
				note.Tag,
				note.Command,
				note.Description,
			})
		}
		out.ShowNotesForTag(result)
	}

}

func listWithNoTag(db *sql.DB) {
	rows, err := db.Query("Select distinct tag from notes")
	checks.CheckErrors(err)
	defer rows.Close()

	result := []string{}

	for rows.Next() {
		note := types.Note{}

		err := rows.Scan(&note.Tag)
		checks.CheckErrors(err)

		result = append(result, note.Tag)
	}
	out.ShowTags(result)
}
