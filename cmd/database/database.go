package database

import (
	"database/sql"
	"fmt"
)

func InitDB(filepath string) *sql.DB {
	db, err := sql.Open("sqlite3", filepath)
	if err != nil || db == nil {
		fmt.Println(err)
		fmt.Println(db)
		panic("Ошибка подключения к базе")
	}
	return db
}

func MigrateDB(db *sql.DB) {
	sql := `
		CREATE TABLE IF NOT EXISTS notes(
			id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
			tag VARCHAR NOT NULL,
			command VARCHAR NOT NULL,
			description VARCHAR NOT NULL
		);
	`

	_, err := db.Exec(sql)
	if err != nil {
		panic(err)
	}
}
