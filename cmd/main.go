package main

import (
	"fmt"
	"os"
	"strconv"
	"ternotes/cmd/database"
	"ternotes/cmd/env"
	"ternotes/cmd/handlers"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	dbPath := "/home/" + os.Getenv("USER") + "/.ternotes/ternotes.sqlite"
	db := database.InitDB(dbPath)
	database.MigrateDB(db)

	inconmingArgs := os.Args[1:]
	if len(inconmingArgs) != 0 {
		switch inconmingArgs[0] {
		case "add":
			handlers.Add(db)
		case "list":
			switch len(inconmingArgs) {
			case 1:
				handlers.List("", db)
			case 2:
				handlers.List(inconmingArgs[1], db)
			default:
				fmt.Println("To much arguments")
			}
		case "delete":
			switch len(inconmingArgs) {
			case 2:
				if i, err := strconv.Atoi(inconmingArgs[1]); err == nil {
					handlers.DeleteID(i, db)
				} else {
					fmt.Println("Wrong arguments")
				}
			case 3:
				handlers.DeleteTAG(inconmingArgs[2], db)
			default:
				fmt.Println("To much arguments")
			}
		case "paste":
			switch len(inconmingArgs) {
			case 2:
				if i, err := strconv.Atoi(inconmingArgs[1]); err == nil {
					handlers.PasteCommand(i, db)
				} else {
					fmt.Println("Wrong arguments")
				}
			default:
				fmt.Println("Wrong arguments")
			}
		default:
			fmt.Println(env.HelloText)
		}
	} else {
		fmt.Println(env.HelloText)
	}

}
